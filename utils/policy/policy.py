import random
import torch
import numpy
import torch.nn.functional as F


class Epsilon_greedy_policy:
    def __init__(self, eps=1, eps_min=0.01, eps_steps=200000):
        self.eps = eps
        self.eps_min = eps_min
        self.eps_decay = (eps - eps_min) / eps_steps
    
    def __reduce_eps(self):
        if self.eps > self.eps_min:
            self.eps -= self.eps_decay

    def choose(self, out):
        self.__reduce_eps()
        if random.uniform(0, 1) <= self.eps:
            # return stochastic_policy
            return random.randrange(out.shape[0])
        return out.argmax().item()

def stochastic_policy(out):    
    with torch.no_grad():
        out = F.softmax(out, dim=0)
    return numpy.random.choice(out.size()[0], p=out.numpy())

def greedy_policy(out):
    return out.argmax().item()