class Exp:
    TRANPOSE_ITEMS = 4

    def __init__(self, state, action, reward, done, comulative_reward):
        self.state = state
        self.action = action
        self.dis_reward = None
        self.last_state = None

        self.curr_reward = reward
        self.comulative_reward = comulative_reward
        self.done = done

    def transpose(self, lis):
        lis[0].append(self.state)
        lis[1].append(self.action)
        lis[2].append(self.dis_reward)
        lis[3].append(self.last_state)

    def __str__(self):
        out = "state: " + str(self.state)
        out += "\naction: " + str(self.action) 
        out += "\nreward: " + str(self.dis_reward) 
        out += "\nlast_state: " + str(self.last_state)
        out += "\ncurr_reward: " + str(self.curr_reward)
        out += "\ndone " + str(self.done)
        out += "\ncom_reward " + str(self.comulative_reward)
        return out
