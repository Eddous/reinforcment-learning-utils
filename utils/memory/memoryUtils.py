def find_final_states(non_transposed_batch):
    final_states = len(non_transposed_batch)
    i = 0
    while i < final_states:
        if non_transposed_batch[i].last_state is None:
            final_states -= 1
            non_transposed_batch[i], non_transposed_batch[final_states] = non_transposed_batch[final_states], non_transposed_batch[i]
        else:
            i += 1
    return final_states

def transpose(batch):
    trans_batch = [list() for _ in range(batch[0].TRANPOSE_ITEMS)]
    for point in batch:
        point.transpose(trans_batch)
    return tuple(trans_batch)