# this is deprecated, will be removed after remaking per, dqn_with_fixed_q_targers

def find_final_states(non_transposed_batch, next_state_position):
    final_states = len(non_transposed_batch)
    i = 0
    while i < final_states:
        if non_transposed_batch[i][next_state_position] is None:
            final_states -= 1
            non_transposed_batch[i], non_transposed_batch[final_states] = non_transposed_batch[final_states], non_transposed_batch[i]
        else:
            i += 1
    return final_states

def transpose(batch):
    trans_batch = [list() for _ in range(len(batch[0]))]
    for point in batch:
        for i in range(len(batch[0])):
            trans_batch[i].append(point[i])
    return tuple(trans_batch)

if __name__ == "__main__":
    batch = [(None,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 0
    assert batch == [(None,)]

    batch = [(1,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 1
    assert batch == [(1,)]

    batch = [(None,), (None,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 0
    assert batch == [(None,), (None,)]

    batch = [(1,), (None,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 1
    assert batch == [(1,), (None,)]

    batch = [(None,), (1,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 1
    assert batch == [(1,), (None,)]

    batch = [(1,), (1,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 2
    assert batch == [(1,), (1,)]

    batch = [(None,), (1,), (None,)]
    final_states = find_final_states(batch, 0)
    assert final_states == 1
    assert batch == [(1,), (None,), (None,)]
