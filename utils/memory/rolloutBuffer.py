from collections import deque
from memory.exp import Exp
import gym


class MultiRolloutBuffer:
    def __init__(self, env_name, num_env, action_selector, rollout, gamma):
        self.buffers = []
        self.counter = -1
        for _ in range(num_env):
            self.buffers.append(RolloutBuffer(env_name, action_selector, rollout, gamma))
    
    def step(self):
        self.counter += 1
        return self.buffers[self.counter % len(self.buffers)].step()
            


class RolloutBuffer:
    # rollout is number of action performed in the advance
    def __init__(self, env_name, action_selector, rollout, gamma):
        self.env = gym.make(env_name)
        self.action_selector = action_selector
        self.rollout = rollout
        self.gamma = gamma
        self.buffer = deque()
        self.comulative_reward = 0
        self.curr_state = self.env.reset()

    def step(self):
        while len(self.buffer) != self.rollout:
            action = self.action_selector(self.curr_state)
            new_state, reward, done, _ = self.env.step(action)
            
            self.comulative_reward += reward

            if done:
                last_state = None
            else:
                last_state = new_state
            tmp = self.gamma
            for exp in reversed(self.buffer):
                if exp.done:
                    break
                exp.last_state = last_state
                exp.dis_reward += tmp * reward
                tmp *= self.gamma

            exp = Exp(self.curr_state, action, reward, done, self.comulative_reward)
            exp.last_state = last_state
            exp.dis_reward = reward

            self.buffer.append(exp)
            if done:
                self.curr_state = self.env.reset()
                self.comulative_reward = 0
            else:
                self.curr_state = new_state
        
        return self.buffer.popleft()


'''
code for testing
'''


class TestEnv:

    def __init__(self):
        self.state = 0

    def step(self, action):
        self.state += 1
        return "state_"+str(self.state), 1, self.state % 9 == 0, None

    def reset(self):
        self.state = 0
        return "state_0"


def test_action_selector(state):
    return "action"


if __name__ == "__main__":
    env = RolloutBuffer(TestEnv(), test_action_selector, 1, 0.99)
    for i in range(10):
        print(env.step())
        print()
