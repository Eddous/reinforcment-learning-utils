import random
from random import choices


class ReplayMemory:
    def __init__(self, capacity, batch_size):
        self.memory = []
        self.capacity = capacity
        self.batch_size = batch_size

        self.slots = None
        self.index = 0
    
    def push(self, experience):
        if self.slots == None:
            self.slots = len(experience)
        if self.slots != len(experience):
            raise Exception("len of experience doesnt match")
        
        if self.index < self.capacity:
            self.memory.append(experience)
        else:
            self.memory[self.index % self.capacity] = experience
        self.index += 1

    def sample(self):
        return choices(self.memory, k=self.batch_size)

