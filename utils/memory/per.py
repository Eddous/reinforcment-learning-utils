from collections import deque
import random
import math


class Replay:

    def __init__(self, capacity, alpha=0.6, e=1e-6, beta=0.4, beta_frames=100000):
        if capacity < 2:
            raise Exception("NOPE capacity have to be >= 2")
        self.memory = [None for i in range(capacity * 2 - 1)]
        self.alpha = alpha
        self.e = e
        self.beta = beta
        self.beta_step = (1 - beta) / 100000

        self.capacity = capacity
        self.index = 0
        self.slots = None

        self.not_processed = deque()
        self.proceding = deque()

        self.indices = None  # inner state

    def __add_to_tree(self, experience, priority):
        experience.append(priority)
        tree_index = self.index % self.capacity + self.capacity - 1
        self.memory[tree_index] = experience
        self.__update_sums(tree_index)
        self.index += 1

    def __get_priority(self, tree_index):
        if self.memory[tree_index] == None:
            return 0
        if isinstance(self.memory[tree_index], list):
            return self.memory[tree_index][-1]
        return self.memory[tree_index]

    def __update_priority(self, tree_index, change):
        if self.memory[tree_index] == None:
            self.memory[tree_index] = 0
        self.memory[tree_index] += change

    def __update_sums(self, tree_index):
        new_sum = self.__get_priority(tree_index)
        new_sum += self.__get_priority(tree_index + 2 * (tree_index % 2) - 1)
        parent_index = (tree_index - 1) // 2
        change = new_sum - self.__get_priority(parent_index)
        while parent_index >= 0:
            self.__update_priority(parent_index, change)
            parent_index = (parent_index - 1) // 2

    def __choose_experience(self):
        index = 0
        rand_number = random.uniform(0, self.memory[0])
        while True:
            if isinstance(self.memory[index], list):
                weight = self.capacity * (self.memory[index][-1] / self.memory[0])
                return index, weight ** -self.beta
            next_index = index * 2 + 1
            if rand_number > self.__get_priority(next_index):
                next_index += 1
            rand_number = self.memory[index] - rand_number
            index = next_index

    def __update_beta(self):
        if self.beta >= 1:
            return
        self.beta += self.beta_step

    def push(self, experience):
        if self.slots == None:
            self.slots = len(experience)
        if self.slots != len(experience):
            raise Exception("len of experience doesnt match")
        self.not_processed.append(experience)
    
    def sample(self, batch_size):
        if self.indices != None:
            raise Exception("probs wasnt updated yet")
        self.indices = deque()
        batch = []
        SI = []
        while len(batch) != batch_size:
            if len(self.not_processed) != 0:
                self.indices.append(None)
                experience = self.not_processed.popleft()
                batch.append(experience)
                self.proceding.append(experience)
                SI.append(0)
                continue
            if self.memory[0] == None:
                break
            index, weight = self.__choose_experience()
            self.indices.append(index)
            SI.append(weight)
            batch.append(self.memory[index])
        self.__update_beta()
        return batch, SI

    def update_probs(self, raw_errors):
        if len(raw_errors) != len(self.indices):
            raise Exception("raw errors is not same len as indices")
        for error in raw_errors:
            error = abs(error) ** self.alpha + self.e
            tree_index = self.indices.popleft()
            if tree_index == None:
                self.__add_to_tree(self.proceding.popleft(), error)
                continue
            # BUG priority of experience, that was added now, has chance to be overwrited
            self.memory[tree_index][-1] = error
            self.__update_sums(tree_index)
        self.indices = None

    def print_all(self):
        print("\nnot processed")
        for i in self.not_processed:
            print(i, end="   ")
        
        print("\nproceding")
        for i in self.proceding:
            print(i, end="   ")
        
        print("\nmemory")
        next_new_line = 0
        for i in range(len(self.memory)):
            print(self.memory[i], end="   ")
            if i == next_new_line:
                next_new_line = next_new_line * 2 + 2
                print()
        print("\n\n")
