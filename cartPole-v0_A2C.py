import torch
from torch.nn import functional as F
import torch.nn as nn
import torch.optim as optim
from A2C import A2C
import gym
from tensorboardX import SummaryWriter
from utils.movingAvarage import MovingAvarage


class NET(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(in_features=4, out_features=8)
        self.policy = nn.Linear(in_features=8, out_features=2)
        self.value = nn.Linear(in_features=8, out_features=1)

    def forward(self, t):
        t = self.fc1(t)
        t = F.relu(t)
        return self.policy(t), self.value(t)

net = NET()
optimazer = optim.Adam(params=net.parameters(), lr=0.01)

a2c = A2C(
    net=net,
    env_name="CartPole-v0",
    num_env=30,
    gamma=0.99,
    rollout=5,
    batch_size=16,
    optimazer=optimazer,
    entropy_beta=0.01)

ma = MovingAvarage(100)
writer = SummaryWriter()

episode = 1
step = 0
mean_reward = 0

while(mean_reward < 195):
    step += 1
    print(mean_reward)
    _, comulative_reward, train, episode_done = a2c.step()
    if train:
        vl, pl, el, a = a2c.training_step()
        writer.add_scalar("value_loss", vl, step)
        writer.add_scalar("policy_loss", pl, step)
        writer.add_scalar("entropy_loss", el, step)
        writer.add_scalar("advantage", a, step)
    if episode_done:
        mean_reward = ma.addNum(comulative_reward)
        writer.add_scalar("reward mean", mean_reward, step)
        episode += 1
writer.close()
print(episode, step)