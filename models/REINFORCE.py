import torch
import torch.nn
import torch.nn.functional as F
import sys
import gym
import policy
import torch.optim as optim
from utils.graphing import Graph
from utils.movingAvarage import MovingAvarage


class Model(torch.nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        # HYPER PARAMETR
        self.fc1 = torch.nn.Linear(in_features=in_features, out_features=64)
        # HYPER PARAMETR
        self.out = torch.nn.Linear(in_features=64, out_features=out_features)

    def forward(self, t):
        t = self.fc1(t)
        t = F.relu(t)
        t = self.out(t)
        return t


def calc_qvals(rewards):
    res = []
    sum_r = 0.0
    for r in reversed(rewards):
        sum_r *= GAMMA
        sum_r += r
        res.append(sum_r)
    return list(reversed(res))


# PARSING ARGS
if len(sys.argv) == 1:
    env_name = "CartPole-v0"
elif len(sys.argv) == 2:
    env_name = sys.argv[1]
else:
    raise Exception("bad args")

# CHECKING IF ENV IS COMPATIBLE
env = gym.make(env_name)
assert len(env.observation_space.shape) == 1, "this env is not compatible with this code due to observation shape"
assert type(
    env.action_space) == gym.spaces.discrete.Discrete, "action space has to be discrete"

# HYPER PARAMS
EPISODES_TO_TRAIN = 5
EPISODES = 500
LR = 0.01
MOVING_AVARAGE = 100
GAMMA = 0.99

model = Model(env.observation_space.shape[0], env.action_space.n)
optimazer = optim.Adam(params=model.parameters(), lr=LR)

# STATISTICS
plot = Graph(expectedPoints=EPISODES)
plot.addTrend("rewards", "points")
plot.addTrend("mov_avarage", "line")
plot.update()
movingAvarage = MovingAvarage(MOVING_AVARAGE)

episode = 0
states, actions, rewards = [], [], []
for _ in range(EPISODES):
    comulative_reward = 0
    episode += 1
    done = False
    curr_rewards = []
    state = env.reset()
    while not done:
        state = torch.from_numpy(state).float()
        states.append(state)
        action = policy.policy.stochastic_policy(model(state))
        actions.append(action)
        state, reward, done, _ = env.step(action)
        curr_rewards.append(reward)
        comulative_reward += reward
    rewards.extend(calc_qvals(curr_rewards))

    plot.addPoint("rewards", comulative_reward)
    plot.addPoint("mov_avarage", movingAvarage.addNum(comulative_reward))
    plot.update()

    if episode % EPISODES_TO_TRAIN != 0:
        continue
    states = torch.stack(states)
    actions = torch.tensor(actions).view(-1, 1)
    rewards = torch.tensor(rewards).view(-1, 1)

    optimazer.zero_grad()

    loss = model(states)
    loss = F.log_softmax(loss)
    #loss = F.softmax(loss)
    loss = loss.gather(index=actions, dim=1)
    loss = rewards * loss
    loss = -loss.sum()
    loss.backward()

    optimazer.step()

    states = []
    actions = []
    rewards = []

env.close()
plot.freeze()
