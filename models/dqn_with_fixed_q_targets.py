import sys
import gym
import math
import random
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from copy import deepcopy
from memory.replayMemory import ReplayMemory
import memory.utils as mu
from utils.graphing import Graph
from utils.movingAvarage import MovingAvarage
from policy import Epsilon_greedy_policy
import policy

class DQN(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        # HYPER PARAMETR
        self.fc1 = nn.Linear(in_features=in_features, out_features=64)
        # HYPER PARAMETR
        self.out = nn.Linear(in_features=64, out_features=out_features)

    def forward(self, t):
        t = self.fc1(t)
        t = F.relu(t)
        t = self.out(t)
        return t

# PARSING ARGS
if len(sys.argv) == 1:
    env_name = "CartPole-v0"
elif len(sys.argv) == 2:
    env_name = sys.argv[1]
else:
    raise Exception("bad args")

# CHECKING IF ENV IS COMPATIBLE
env = gym.make(env_name)
assert len(env.observation_space.shape) == 1, "this env is not compatible with this code due to observation shape"
assert type(env.action_space) == gym.spaces.discrete.Discrete, "action space has to be discrete"

# HYPER PARAMETRS
BATCH_SIZE = 64
GAMMA = 0.999
TARGET_UPDATE = 10
MEMORY_SIZE = 1000000
LR = 0.01
EPISODES = 1000
MOVING_AVARAGE = 100

# MAKING MODEL
policy_net = DQN(env.observation_space.shape[0], env.action_space.n)
target_net = DQN(env.observation_space.shape[0], env.action_space.n)
target_net.eval()
optimazer = optim.Adam(params=policy_net.parameters(), lr=LR)

# POLICY
# policy = Epsilon_greedy_policy()

# MEMORY
replay = ReplayMemory(MEMORY_SIZE, BATCH_SIZE)

# STATISTICS
plot = Graph(expectedPoints=EPISODES)
plot.addTrend("rewards", "points")
plot.addTrend("mov_avarage", "line")
plot.update()
movingAvarage = MovingAvarage(MOVING_AVARAGE)

for episode in range(EPISODES):
    if episode % TARGET_UPDATE == 0:
        target_net.load_state_dict(policy_net.state_dict())
    state = torch.from_numpy(env.reset()).float()
    done = False
    comulative_reward = 0
    while not done:
        env.render()
        
        # CHOOSE ACTION
        with torch.no_grad():
            out = policy_net(state)
        action = policy.stochastic_policy(out)
        
        # MAKE TUPLE
        last_state = state
        state, reward, done, _ = env.step(action)
        comulative_reward += reward
        state = torch.from_numpy(state).float()
        if done:
            state = None
        replay.push((last_state, action, reward, state))

        # SAMPLE
        batch = replay.sample()
        index = mu.find_final_states(batch, 3)
        
        states, actions, rewards, next_states = mu.transpose(batch)
        states = torch.stack(states)
        actions = torch.tensor(actions).view(BATCH_SIZE, 1)
        rewards = torch.tensor(rewards).view(BATCH_SIZE, 1)
        next_states = torch.stack(next_states[:index])

        # TRAIN
        current = policy_net(states).gather(dim=1, index=actions)
        
        target = target_net(next_states).max(dim=1).values.unsqueeze(1)
        target = torch.cat((target, torch.zeros(BATCH_SIZE-index, 1)), 0)
        target = rewards + target * GAMMA
        
        loss = F.mse_loss(current, target)
        optimazer.zero_grad()
        loss.backward()
        optimazer.step()

    # PLOTING
    tmp = movingAvarage.addNum(comulative_reward)
    plot.addPoint("rewards", comulative_reward)
    plot.addPoint("mov_avarage", tmp)
    plot.update()

    # OUTPUT
    print(comulative_reward)

env.close()
plot.freeze()
