import torch
import policy
from memory import memoryUtils as mu
from memory.rolloutBuffer import MultiRolloutBuffer as MultiRolloutBuffer
from torch.nn import functional as F


class A2C:
    def __init__(self, net, env_name, num_env, gamma, rollout, batch_size, optimazer, entropy_beta):
        self.net = net
        self.gamma = gamma
        self.rollout = rollout
        self.batch_size = batch_size
        self.optimazer = optimazer
        self.entropy_beta = entropy_beta

        self.env_wrapper = MultiRolloutBuffer(
            env_name, num_env, self.action_selector, rollout, gamma)
        self.batch = []

    def action_selector(self, state):
        # is arg for RolloutBuffer, function describes how to get action from net
        with torch.no_grad():
            return policy.stochastic_policy(self.net(torch.tensor(state).float())[0])

    def step(self):
        # returns: reward agent have obtained this step, if batch is ready for training
        self.batch.append(self.env_wrapper.step())
        return self.batch[-1].curr_reward, self.batch[-1].comulative_reward, len(self.batch) >= self.batch_size, self.batch[-1].done

    def training_step(self):
        index = mu.find_final_states(self.batch)
        states, actions, dis_rewards, last_states = mu.transpose(self.batch)

        # beware of https://github.com/pytorch/pytorch/issues/13918
        states = torch.tensor(states).float()
        actions = torch.tensor(actions).view(-1, 1)
        dis_rewards = torch.tensor(dis_rewards).view(-1, 1)
        last_states = torch.tensor(last_states[:index]).float()

        # Compute current value of state
        with torch.no_grad():
            state_val = torch.zeros(len(self.batch)-index, 1)
            if last_states.nelement() != 0:
                state_val = torch.cat((self.net(last_states)[1], state_val), 0)
            state_val = dis_rewards + (self.gamma ** self.rollout) * state_val

        policy_out, state_val_out = self.net(states)
        softmax_policy_out = F.softmax(policy_out, dim=1)
        log_softmax_policy_out = softmax_policy_out.log()
        actions_taken = log_softmax_policy_out.gather(index=actions, dim=1)

        val_loss = F.mse_loss(state_val_out, state_val)
        advantage = state_val - state_val_out.detach()
        # policy has to be maximalized
        policy_loss = -(advantage * actions_taken).mean()
        # entropy has to be maximalized, entropy_loss is -entropy
        entropy_loss = self.entropy_beta * \
            (log_softmax_policy_out * softmax_policy_out).sum()
        loss = val_loss + policy_loss + entropy_loss

        self.optimazer.zero_grad()
        loss.backward()
        self.optimazer.step()

        self.batch = []
        # TODO kl-div, l2
        return (val_loss.item(),
                policy_loss.item(),
                entropy_loss.item(),
                advantage.mean().item())
